#!/bin/sh
#
# NASA: Astronomy Picture of the Day
# https://apod.nasa.gov/apod/astropix.html

function get_timer() {
	# see https://wiki.archlinux.org/index.php/Systemd/Timers#Realtime_timer
	echo "*-*-* 7:00"	# every day on 7:00 (publication time is unknown, USA)
}

function get_url() {
	BASE_URL="https://apod.nasa.gov/apod/"
	URL="${BASE_URL}astropix.html"
	URL=$(curl -s "${URL}" | (echo -n "${BASE_URL}" ; grep -o -m 1 '[^"]*image/[0-9]\{4,\}/[^"]*'))
	echo "${URL}"
}

while [[ -n "${1}" ]]; do
	case "${1}" in
	"--url") get_url ;;
	"--timer") get_timer ;;
	esac
	shift
done
