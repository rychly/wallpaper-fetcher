#!/bin/sh
#
# NASA: Image of the Day
# https://www.nasa.gov/multimedia/imagegallery/iotd.html

function get_timer() {
	# see https://wiki.archlinux.org/index.php/Systemd/Timers#Realtime_timer
	echo "*-*-* 7:00"	# every day on 7:00 (publication time is unknown, USA)
}

function get_url() {
	BASE_URL="https://www.nasa.gov/"
	URL="${BASE_URL}rss/dyn/lg_image_of_the_day.rss"
	URL=$(curl -s "${URL}" | grep -o -m 1 '<enclosure url="[^"]*' | cut -d '"' -f 2)
	echo "${URL}"
}

while [[ -n "${1}" ]]; do
	case "${1}" in
	"--url") get_url ;;
	"--timer") get_timer ;;
	esac
	shift
done
