#!/bin/sh
#
# ESA/Hubble: Picture of the Week
# https://www.spacetelescope.org/images/potw/

function get_timer() {
	# see https://wiki.archlinux.org/index.php/Systemd/Timers#Realtime_timer
	echo "Mon 6:15"	# each Monday on 6:15 (publication time is 6:00, Germany)
}

function get_url() {
	BASE_URL="https://www.spacetelescope.org"
	URL="${BASE_URL}/images/potw/"
	URL=$(curl -s "${URL}" | (echo -n "${BASE_URL}" ; grep -o -m 1 '/images/potw[0-9]\{4\}./' ))
	URL=$(curl -s "${URL}" | grep -o -m 1 '[^"]*/images/large/potw[0-9]\{4\}.[^"]*')
	echo "${URL}"
}

while [[ -n "${1}" ]]; do
	case "${1}" in
	"--url") get_url ;;
	"--timer") get_timer ;;
	esac
	shift
done
